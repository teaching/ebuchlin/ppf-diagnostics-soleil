# Instrumentation, diagnostics, traitement du signal
# TP Analyse du Soleil par spectroscopie UV

[Éric Buchlin](mailto:eric.buchlin@cnrs.fr), 20 septembre 2024

L'objectif de ce TP est de déterminer différents paramètres du plasma de la couronne solaire, grâce à la spectroscopie, et plus particulièrement grâce aux raies émises en UV. Nous utiliserons pour cela des observations par:

* des **spectromètres à fente**: en chaque position le long d'une fente, la lumière est dispersée par un réseau, et on obtient une intensité en fonction de la longueur d'onde. Le déplacement de la fente permet d'obtenir une image.
* des **imageurs à bande étroite**: une image est obtenue en sélectionnant une certaine bande en longueur d'onde; cette bande est en général choisie, autant que possible, pour qu'elle soit dominée par une seule raie.

Les questions auxquelles il faut répondre dans le compte-rendu sont notées **[Qn]**. Les autres choses à faire, mais pour lesquelles il n'est pas nécessaire d'avoir une trace dans le compte-rendu, sont notées [An]. Le compte-rendu peut être rendu par e-mail (ou service de dépôt de fichier) au format PDF ou notebook python jusqu'au 29 septembre 2024 minuit.


## Premier contact avec les données disponibles

Vous avez un compte sur les machines `edu-calcul1` et `edu-calcul2`, sur lesquelles IDL et Python sont disponibles (Matlab est aussi disponible sur la première).
En Python, utiliser de préférence Python3 (commande `python3`, et shell interactif `ipython3`).
Les répertoires des utilisateurs sont partagés entre votre terminal (PC) et les serveur, donc vous pouvez éditer votre code sur votre terminal, et le faire tourner sur un serveur.

Les chemins ci-dessous sont relatifs au répertoire `/home/ebuchlin/tp-plasmas`. Les données prennent beaucoup de place, donc faut lire directement les fichiers de ce répertoire, sans les recopier dans vos comptes.

Le répertoire `data/` contient deux sous-répertoires:

* `AIA/` pour des données de l'instrument [AIA](http://aia.lmsal.com/) du satellite américain SDO (lancé en 2010 en orbit géosynchrone), dans plusieurs bandes (identifiées par un nombre correspondant à une longueur d'onde en ångströms). AIA prend des images du Soleil dans des bandes étroites autour de différentes raies UV émises dans l'atmosphère solaire. Ces données sont de «niveau 1.5», c'est-à-dire que des traitements de calibration de base (dark current, flat field...) ont déjà été appliqués, mais que les intensités sont encore en «data numbers» (DN) au lieu d'être en unités physiques.
* `EIS/` pour des données de l'instrument [EIS](http://msslxr.mssl.ucl.ac.uk:8080/SolarB/Solar-B.jsp) du satellite japonais Hinode (lancé en 2006 en orbit terrestre basse). EIS est un spectromètre à fente, dans le domaine de l'UV extrême. Le sous-répertoire `L1/` contient les données de «niveau 1» (après dark current, flat field...), pour 5 balayages successifs d'un certain champ de vue.

Les fichiers de données sont en format [FITS](https://fits.gsfc.nasa.gov/), un format standardisé par la NASA et très utilisé en astronomie; il comportent une en-tête avec des métadonnées en format texte sur 80 colonnes, puis une ou plusieurs tables de données (avec leurs en-têtes, elles aussi en format texte). Les images en format FITS sont lisibles avec les logiciel [`ds9`](http://ds9.si.edu/site/Home.html) ou [fv](https://heasarc.gsfc.nasa.gov/ftools/fv/).


**[Q1]** D'après les en-têtes du fichier EIS `EIS/L1/eis_l1_20110102_125317.fits`, donner l'intervalle de temps pendant lequel l'observation a été faite (pendant ce temps la fente du spectroscope a scanné le champ observé), la taille d'un pixel, le champ de vue, quelques longueurs d'onde disponibles

> Indications: utiliser `less` dans un terminal, et voir les mots-clés: `DATE_OBS`, `DATE_END`, `XCEN`, `FOVX`, `CDELT1`, `CTYPE1`, `TTYPE1`....

**[Q2]** Calculer le milieu de cet intervalle de temps, et déterminer le fichier FITS correspondant à l'observation d'AIA (dans la bande à 193Å) la plus proche de ce moment.

[A3] Afficher des fichiers FITS d'AIA, à plusieurs instants et dans plusieurs bandes en longueur d'onde (193Å, 304Å...), dans le logiciel `ds9` ou `fv`. Ces données peuvent aussi être explorées, de manière plus pratique mais en moins bonne qualité, à partir de [HelioViewer](http://helioviewer.ias.u-psud.fr/).

**[Q4]** Indiquer, sur l'image AIA sélectionnée à la [Q2], le champ de vue observé par EIS [Q1].

> Indications: calculer la position en pixels sur l'image AIA correspondant au centre du champ de vue EIS déterminé à [Q1], en sachant qu'un pixel (*i*,*j*) dans une image a pour coordonnées (`CRVAL1`+(*i*-`CRPIX1`)×`CDELT1`, `CRVAL2`+(*j*-`CRPIX2`)×`CDELT2`); ouvrir l'image AIA avec `ds9` ou `fv` et repérer alors la position du centre du champ de vue d'EIS.


## Carte d'intensité avec EIS

Les bibliothèques spécialisées nécessaires au traitement des fichiers FITS originaux de Hinode/EIS ne sont pas disponibles sur les machines sur lesquelles vous avez des comptes, donc le TP utilisera des données sauvegardées par IDL (format propriétaire «IDL save», extension `.sav`), résultats d'un pré-traitement avec ces bibliothèques spécialisées.

Ces fichiers sont lisibles de Python par la fonction `readsav` du module `scipy.io.idl`.

Les données de Hinode/EIS de niveau 1 sont ainsi disponibles dans `data/EIS/L1/`, dans des fichiers dont le nom est du type `eis_l1_20110102_125317_09.sav`: après `l1`, le nom du fichier indique la date et l'heure, ainsi que le numéro de la bande spectrale, selon la table suivante.

| N. | Raie principale  | log T<sub>max</sub> |
|----|------------------|---------------------|
| 0  | Fe X 184.54Å     | 6.0                 |
| 1  | Fe VIII 185.21Å  | 5.6                 |
| 2  | Fe XII 186.88Å   | 6.1                 |
| 3  | Fe XI 188.23Å    | 6.1                 |
| 4  | Fe XXIV 191.93Å  | 7.3                 |
| 5  | Ca XVII 192.82Å  | 6.7                 |
| 6  | Fe XII 195.12Å   | 6.1                 |
| 7  | Fe XIII 196.54Å  | 6.2                 |
| 8  | Fe XIII 202.04Å  | 6.2                 |
| 9  | Fe XIII 203.83Å  | 6.2                 |
| 10 | Fe XXIV 255.10Å  | 7.3                 |
| 11 | He II 256.32Å    | 4.7                 |
| 12 | Fe XVI 262.98Å   | 6.4                 |
| 13 | Fe XXIII 263.79Å | 6.6                 |
| 14 | Fe XIV 264.68Å   | 6.3                 |
| 15 | Fe XIV 274.20Å   | 6.3                 |
| 16 | Si VII 275.35Å   | 5.8                 |
| 17 | Fe XV 284.16Å    | 6.3                 |


[A5] Lire un de ces fichiers dans Python ou IDL, pour la fenêtre numéro 6 (Fe XII 195.12Å). On obtient la variable `d`. Explorer la structure de cette variable.

> En Python (dans un shell iPython):

> ```python
> from scipy.io.idl import readsav
> d = readsav('..._06.sav')
> d.wininfo      # informations sur toutes les fenêtres
> d.d.wvl[0]     # axe de longueur d'onde [Å]
> d.d.time[0]    # axe de temps [s] depuis le début du scan
> d.d.solar_x[0] # axe des x (coordonnées héliocentriques) [arcsec]
> d.d.solar_y[0] # axe des y (coordonnées héliocentriques) [arcsec]
> d.d.int[0]     # intensité en fonction de la position et de la longueur d'onde
> ...
> ```

> En IDL: `help, d, /struct`, puis `help, d.int, /struct`, etc.

> Remarque: à chaque instant on observe à une certaine position *x*, l'axe des temps et l'axe des *x* sont donc en correspondance.


**[Q6]**  Calculer une carte de l'intensité intégrée dans la raie Fe XII à 19.512nm. Quelles structures de la couronne solaire (telles que vues dans AIA, voir [Q4]) sont visibles dans le champ de vue de EIS?

> Il s'agit de calculer en chaque position l'intensité intégrée dans la raie.
> Une façon simple de faire est de sommer le long de l'axe de longueur d'onde
> (il vaudrait mieux utiliser une méthode plus avancée d'ajustement de raie, mais nous n'avons pas le temps pour cela).

> Squelette de code Python:

> ```python
> import numpy as np
> import matplotlib.pyplot as plt
> from scipy.io.idl import readsav
>
> d = readsav('..._06.sav')
> i = d.d.int[0]    # cube d'intensité spectrale
> x = d.d.solar_x[0]
> y = d.d.solar_y[0]
> ii = np.sum(..., axis=...)  # sommer sur l'axe des λ
>
> plt.imshow(ii, origin='lower',
>            vmin=..., vmax=...,
>            extent=[x[0], x[-1], y[0], y[-1]])
> plt.colorbar()
> plt.show()
> ```


## Carte de densité avec EIS

Pour un ion donné, l'intensité de chaque raie est proportionnelle à son «émissivité», qui est fonction de la densité électronique *n<sub>e</sub>*.
Ces émissivités pour 8377 raies différentes du Fe XII, issues de la base de données de physique atomique [CHIANTI](http://www.chiantidatabase.org/), sont stockées dans la variable `fe12e` qui peut être lue dans le fichier `data/EIS/fe12e.sav`.

En Python, les émissivités pour la raie d'indice `n` sont ainsi accessibles par:

```python
e = readsav('fe12e.sav')
dens = e.fe12e.density[0]         # tableau de densité
em = e.fe12e.emiss[0][n].em[:,0]  # tableau de l'émissivité de la raie en fonction de la densité
```

Les raies qui nous intéressent ici sont

* Fe XII à 195.119Å, d'indice *n*=873 dans la liste des raies pour les émissivités
* Fe XII à 186.887Å, d'indice *n*=750 dans la liste des raies pour les émissivités

**[Q7]** Tracer le rapport des émissivités de ces deux raies en fonction de la densité électronique.

> Indications: tracer le rapport en log-log (Python: `plt.loglog(...)`); mettre des étiquettes sur les axes (Python: `plt.xlabel(...)`, `plt.ylabel(...)`).

**[Q8]** Produire une carte de densité dans le champ de vue de EIS.

> Indications: lire les données EIS correspondant aux deux raies à utiliser; créer une carte d'intensité intégrée (voir [Q6]) pour chacune d'elles; utiliser `np.interp()` en Python ou `interpol()` en IDL pour interpoler la relation entre le rapport des intensités (égal au rapport des émissivités) et la densité.

> Attention: le deuxième argument de `np.interp()` doit être strictement croissant.


## Vitesses Doppler avec EIS

**[Q9]** Calculer les cartes de la vitesse Doppler dans les raies Fe XII 195.119Å et Ca XVII 192.82Å, déduites des données spectroscopiques, pour la première observation d'EIS.

> Indications: pour simplifier, on pourra calculer en chaque position le centre de gravité de la raie dans la fenêtre en longueur d'onde, soustraire la longueur d'onde au repos de la raie, et calculer la vitesse Doppler correspondante (*v*/*c*=Δλ/λ<sub>0</sub>).

> En Python, la première étape (centre de gravité) se calcule très simplement pour l'ensemble des positions (x, y) avec les fonctions `np.dot()` et `np.sum()`.


Une difficulté dans une telle détermination de la vitesse Doppler est que l'étalonnage donnant l'axe de longueur d'onde est en fait sensible à différents effets, dépendant en gros de la position du satellite Hinode sur son orbite (orbite basse, [héliosynchrone](https://fr.wikipedia.org/wiki/Orbite_h%C3%A9liosynchrone), période d'environ 94min), notamment:

* l'effet Doppler dû au mouvement du satellite autour de la Terre;
* l'éclairement du satellite par la Terre varie (notamment en infrarouge), d'où des effets thermiques sur la structure mécanique du spectroscope.

Cela conduit à une variation temporelle de la position de chaque raie, qui se superpose aux variations d'origine solaire.

**[Q10]** Mettre en évidence cette variation temporelle, en traçant la vitesse Doppler pour la raie Fe XII 195.119Å, moyennée le long de la fente du spectromètre, en fonction du temps.

> Indications: il faut ouvrir successivement les 5 fichiers de données pour cette raie, calculer la carte d'effet Doppler correspondante (comme dans la question précédente), moyenner le long de la fente (Python: `np.mean()`), et tracer cette moyenne en fonction du temps (sur le même graphique pour ).

> Une difficulté est d'avoir un axe des temps commun à toutes les 5 observations, ce qui se contourne en se donnant la liste des noms de fichiers et en calculant la durée en secondes entre 12:00:00 et le début de l'observation.

> ```python
> files = ['125317', '131848', '134422', '140956', '143530']
> for f in files:
>    d = readsav(...)
>    t0 = int(f[4:6]) + int(f[2:4]) * 60 + (int(f[0:2]) - 12) * 3600
>    ...
>    dopp = ...
>    plt.plot(t0 + d.d.time[0], dopp)
> ...
> plt.show()
> ```

> Bien sûr il existe de meilleures manière de gérer le temps en Python (`datetime`, `astropy.time`...), mais nous n'avons pas le temps pour cela.


**[Q11]** À partir de ce que vous savez de l'orbite d'Hinode, estimer l'ordre de grandeur de la vitesse Doppler maximale correspondant au mouvement du satellite autour de la Terre. Est-ce que ce mouvement suffit à expliquer les variations de vitesse Doppler mesurées?

**[Q12]** Une correction (correspondant aux effets instrumentaux connus) de la longueur d'onde en fonction du temps est proposée dans les fichiers de Hinode/EIS (variable `d.wave_corr_t[0]` dans les fichiers `.sav`): la tracer, et expliquer si elle permet effectivement de corriger la variation temporelle mesurée à [Q10]. Si oui, l'utiliser pour produire des cartes d'effet Doppler corrigées. A-t-on en moyenne plus de décalages vers le rouge ou vers le bleu?

> Indication: il faut soustraire `d.wave_corr_t[0]` aux décalages Doppler mesurés (en ångströms).

> Pour être plus précis, les corrections suivantes sont aussi disponibles:

> * `d.wave_corr_tilt[0]`, dépendant de la position le long de la fente
> * `d.wave_corr[0]`, pour chaque position de pixel; ceci est la somme de `d.wave_corr_t[0]` sur l'axe temporel (ou des *x*) et de `d.wave_corr_tilt[0]` le long de la fente (axe des *y*).

<!--
## Détermination de la température de la couronne

La table ci-dessous donne les températures typiques d'émissions des principales raies contenues dans les différentes bandes d'AIA (les noms des canaux correspondent à la longueur d'onde approximative en ångströms).

| Canal | Raie principale  | log T<sub>max</sub> |
|-------|------------------|---------------------|
| 304   | He II            | 4.7                 |
| 171   | Fe IX            | 5.8                 |
| 193   | Fe XII, XXIV     | 6.1, 7.3            |
| 211   | Fe XIV           | 6.3                 |
| 335   | Fe XVI           | 6.4                 |
|  94   | Fe XVIII         | 6.8                 |
| 131   | Fe VI, XX, XXIII | 5.6, 7.0, 7.2       |


En fait, l'intensité mesurée dans chaque bande dépend de la *réponse en longueur d'onde* de l'instrument et des intensités reçues dans l'ensemble des raies spectroscopiques, elles-mêmes dépendant (dans l'approximation coronale) de la quantité de plasma (le long de la ligne de visée dans un pixel) à une certaine densité et température.
Ces informations peuvent être combinées pour donner la *réponse en température* de l'instrument, qui représente pour une certaine bande de l'instrument et pour chaque température l'intensité mesurée quand une certaine quantité de plasma à cette température est présente dans le volume constitué par un pixel et la ligne de visée.

Le fichier `data/AIA/aia_response.sav` contient la réponse en longueur d'onde (variable `wvlresp`) et en température (variable `tempresp`) d'AIA, dans chacun de ses canaux, en fonction respectivement de la longueur d'onde (en ångströms) et de la température (logarithme en base 10, kelvins).

**[Q13]** Tracer (en échelle logarithmique) les courbes de réponse en température des différentes bandes de SDO/AIA.

> Pour avoir les données utiles en Python, par exemple pour le canal à 211Å:

> ```python
> r = readsav('aia_response.sav')
> logt = r.tempresp.logte[0]
> r211 = r.tempresp.a211[0].tresp[0]
> ```

**[Q14]** En supposant le plasma isotherme et en utilisant le rapport entre les intensités dans deux canaux bien choisis de AIA, estimer la température du plasma.

> Indications: pour un plasma isotherme (à la température *T*), l'intensité mesurée dans chaque canal est proportionnelle à la réponse en température pour ce canal à la température *T*.

**[Q15]** En utilisant simultanément deux rapports d'intensités entre canaux de AIA, montrer que le plasma n'est *pas* partout isotherme.

Une détermination plus précise de la température (et plus généralement, de la DEM, représentant la distribution du plasma en fonction de la température) pourrait être obtenue en utilisant un grand nombre d'intensités de raies provenant d'un spectromètre comme EIS.
-->

Acronymes:

* AIA: Atmospheric Imaging Assembly
* SDO: Solar Dynamics Observatory
* EIS: Extreme-UV Imaging Spectrometer
* DEM: Differential Emission Measure
