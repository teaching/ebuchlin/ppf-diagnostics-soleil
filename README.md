# UE Instrumentation, diagnostics, traitement du signal

# TP analyse du Soleil par spectroscopie UV

* Date: 20 septembre 2024
* Lieu: [Institut d'Astrophysique Spatiale](http://www.ias.u-psud.fr/), Bâtiment 121, Université Paris-Saclay, Orsay. [Comment venir?](https://www.ias.u-psud.fr/fr/le-laboratoire/guide-du-visiteur/plans-et-indications)
  * 9h: présentation des méthodes de diagnostics des plasmas solaires, en salle 4/5 (étage 2, à droite en venant de l'escalier principal).
  * Ensuite, TP, en salle informatique (étage 2, à droite en venant de l'escalier principal).
* Sujet du TP: [français](./tp-fr.md), [anglais](./tp-en.md)
* Comptes-rendus à rendre avant le 29 septembre minuit.
